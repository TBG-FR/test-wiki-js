---
title: RocketChat
description: Documentation liée à RocketChat
published: true
date: 2021-05-05T10:13:10.609Z
tags: rocketchat, public
editor: markdown
dateCreated: 2021-05-05T09:47:50.733Z
---

# RocketChat

- [update-version](/rocketchat/update-version)
- [macOS](/install/macos)
- [Windows](/install/windows)
{.links-list}